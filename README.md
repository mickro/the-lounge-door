# The Lounge Door

## Features

- standalone client to connect to The Lounge server
- the lounge trayicon
- trayicon is blinking when you have highlighted message
- trayicon has a tooltip which shows message counters /network /channel
- autoconnect option

## Compatibility

The lounge door `v1.0.0` is compatible with the lounge `v3.x`

## How to use?

### Install The lounge

You should install _the lounge_ on your server/machine

- web site: https://thelounge.chat/
- install instructions: https://thelounge.chat/docs/install-and-upgrade

### build a door

- clone this repository
- `npm i`
- `yarn dist`
- run the `dist/the-lounge-door-x.x.x` file

## How to contribute?

### Create your own cool feature

Basically play with the code, and run `npm start`.

### Follow the roadmap

There is a TODO file somewhere.

### Follow unique coding spirit

Use vanilla.js, create new features optionals.

... and share by forking the whole project or making a MR
