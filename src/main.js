const { app, BrowserWindow, Menu, Tray, ipcMain } = require("electron");
const path = require("path");
const fs = require("fs");

const collector = fs.readFileSync(
  path.join(__dirname, "web/collector.js"),
  "utf8"
);

const coloredIcon = path.join(__dirname, "../assets/the-lounge-64.png");
const bwIcon = path.join(__dirname, "../assets/the-lounge-BW-64.png");

let win;
let tray;
let isColoredIconTray = false;
let hMsgCount = 0;
let nMsgCount = 0;
const tooltipTitle = "Oo. The Lounge .oO";
let maxLineLength = tooltipTitle.length;
let oldToolTip = tooltipTitle;

function createWindow() {
  win = new BrowserWindow({
    autoHideMenuBar: true,
    webPreferences: {
      nodeIntegration: true
    }
  });

  // and load the index.html of the app.
  win.loadURL(`file://${__dirname}/web/index.html?step=launch`);

  win.on("closed", () => {
    win = null;
  });

  require("./menu");
}

function createTray() {
  tray = new Tray(bwIcon);
  tray.setToolTip(tooltipTitle);
  tray.on("click", () => {
    if (win.isMinimized()) {
      win.restore();
      win.focus();
      return;
    }

    if (!win.isFocused()) {
      win.focus();
      return;
    }

    win.minimize();
  });
}

app.on("ready", () => {
  createTray();
  createWindow();
});

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (win === null) {
    createWindow();
  }
});

const startCollecting = () => {
  if (typeof collector == "string") {
    win.webContents.executeJavaScript(collector);
    win.webContents.executeJavaScript(
      "try{_collector.start();}catch(err){console.error('Error on start');console.error({err});}"
    );
  } else console.log("no collector init");
};

ipcMain.on("open-door", (event, url) => {
  win.webContents.on("dom-ready", startCollecting);
  win.loadURL(url);
});

const updateMaxLength = lineLength => {
  if (lineLength > maxLineLength) {
    maxLineLength = lineLength;
  }
};

const formatNetworkLineTooltip = (name, nick) => {
  const line = `... ${nick} @ ${name}`;
  updateMaxLength(line.length);
  return line;
};

const formatChanLineTooltip = (name, count, isHighlighted, isActive) => {
  const line = `${isActive ? "> " : ""}${
    isHighlighted ? "! " : ""
  }${name}: ${count}`;
  updateMaxLength(line.length);
  return line;
};

const getChannelCounts = args => {
  args.network.channels.push(...args.network.queries);
  const counts = args.network.channels;

  if (counts.length > 0) {
    const channels = counts.filter(c => c.count);
    if (
      channels.length > 0 ||
      (nMsgCount < args.nMsgCount &&
        args.activeChanName &&
        args.activeNetworkName === args.network.name)
    ) {
      // all channels but active
      const chans = channels.map(chan =>
        formatChanLineTooltip(chan.name, chan.count, chan.isHighlighted)
      );

      // active channel
      if (
        nMsgCount < args.nMsgCount &&
        args.activeChanName &&
        args.activeNetworkName
      ) {
        if (args.activeNetworkName === args.network.name)
          chans.unshift(
            formatChanLineTooltip(
              args.activeChanName,
              args.nMsgCount - nMsgCount,
              args.activeChanHighlighted,
              true
            )
          );
      }

      // network name
      chans.unshift(
        formatNetworkLineTooltip(args.network.name, args.network.nick)
      );

      return chans;
    }
  }
  return [];
};

ipcMain.on("collector-tick", (_, args) => {
  try {
    const activeChanHighlighted = hMsgCount < args.hMsgCount;

    // update tooltip with message counts
    let newTooltip = tooltipTitle;
    if (args.networks && args.networks.length > 0) {
      const chans = [];
      maxLineLength = tooltipTitle.length;
      for (let i = 0; i < args.networks.length; ++i) {
        const networkCounts = getChannelCounts({
          network: args.networks[i],
          nMsgCount: args.nMsgCount,
          activeChanName: args.activeChanName,
          activeNetworkName: args.activeNetworkName,
          activeChanHighlighted
        });

        if (networkCounts.length > 0) {
          chans.push(
            "", // to add a new line before network counts
            ...networkCounts
          );
        }
      }

      const spacesCount = Math.floor(maxLineLength - tooltipTitle.length);
      const spacesToCenter = " ".repeat(spacesCount);

      newTooltip = `${spacesToCenter}${tooltipTitle}\n${chans.join("\n")}`;
    }

    if (newTooltip !== oldToolTip) {
      tray.setToolTip(newTooltip);
      oldToolTip = newTooltip;
    }

    // update icon if any highlight required
    if (win.isFocused()) {
      hMsgCount = args.hMsgCount;
      nMsgCount = args.nMsgCount;
      if (!isColoredIconTray) {
        tray.setImage(coloredIcon);
        isColoredIconTray = true;
      }
    } else {
      if (args.hasHighlight || activeChanHighlighted) {
        if (isColoredIconTray) {
          tray.setImage(bwIcon);
          isColoredIconTray = false;
        } else {
          tray.setImage(coloredIcon);
          isColoredIconTray = true;
        }
      }
    }
  } catch (err) {
    console.error(err);
    win.webContents.executeJavaScript("try{_collector.stop();}catch(err){};");
  }
});
