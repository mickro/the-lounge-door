const { Menu } = require("electron");
const electron = require("electron");
const app = electron.app;

const template = [
  {
    label: "Menu",
    submenu: [
      {
        label: "Configuration panel",
        accelerator:
          process.platform === "darwin" ? "Alt+Command+H" : "Ctrl+Shift+H",
        click(item, focusedWindow) {
          if (focusedWindow)
            focusedWindow.loadURL(
              `file://${__dirname}/web/index.html?step=configuration`
            );
        }
      },
      {
        type: "separator"
      },
      {
        label: "Reload",
        accelerator: "CmdOrCtrl+R",
        click(item, focusedWindow) {
          if (focusedWindow) focusedWindow.reload();
        }
      },
      {
        label: "Toggle Developer Tools",
        accelerator:
          process.platform === "darwin" ? "Alt+Command+I" : "Ctrl+Shift+I",
        click(item, focusedWindow) {
          if (focusedWindow) focusedWindow.webContents.toggleDevTools();
        }
      },
      {
        type: "separator"
      },
      {
        role: "quit"
      }
    ]
  }
];

const menu = Menu.buildFromTemplate(template);
Menu.setApplicationMenu(menu);
