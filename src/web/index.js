const { ipcRenderer } = require("electron");

window.app = {
  inputIdList: ["protocol", "url", "port"],
  autoconnectId: "autoconnect",

  getFullUrl: () => {
    const protocol = document.getElementById("protocol").value;
    const url = document.getElementById("url").value;
    const port = document.getElementById("port").value;
    return `${protocol}://${url}:${port}`;
  },

  setFeedbackMessage: msg => {
    const feedbackBox = document.getElementById("feedback");
    if (feedbackBox) {
      feedback.innerHTML = msg;
    } else {
      console.log(`feedback: ${msg}`);
    }
  },

  loadData: () => {
    app.inputIdList.forEach(inputId => {
      const data = localStorage.getItem(inputId);
      if (data) document.getElementById(inputId).value = data;
    });
    if (localStorage.getItem(app.autoconnectId))
      document.getElementById(app.autoconnectId).checked = true;
  },

  saveData: () => {
    app.inputIdList.forEach(inputId =>
      localStorage.setItem(inputId, document.getElementById(inputId).value)
    );
    document.getElementById(app.autoconnectId).checked
      ? localStorage.setItem(app.autoconnectId, true)
      : localStorage.removeItem(app.autoconnectId);
  },

  openDoor: event => {
    if (event) event.preventDefault();
    console.log("openDoor");
    try {
      app.saveData();
      ipcRenderer.send("open-door", app.getFullUrl());
    } catch (err) {
      app.setFeedbackMessage("check your values");
    }
  },

  inputFeedback: () => {
    app.setFeedbackMessage(app.getFullUrl());
    // TODO: check validity => enable button
  },

  init: () => {
    try {
      document
        .getElementById("form")
        .addEventListener("submit", app.openDoor, true);

      app.inputIdList.forEach(inputId =>
        document
          .getElementById(inputId)
          .addEventListener("input", app.inputFeedback, false)
      );

      app.loadData();
      app.inputFeedback();
    } catch (err) {
      app.setFeedbackMessage("code is wrong somewhere around init()!");
      console.error({ err });
    }
  },

  start: () => {
    const urlParams = new URLSearchParams(global.location.search);
    const step = urlParams.get("step");
    if (step === "launch" && localStorage.getItem(app.autoconnectId)) {
      app.openDoor();
    } else {
      app.show();
    }
  },

  show: () => {
    document.getElementById("loader").style.visibility = "hidden";
    document.getElementById("config").style.visibility = "visible";
  }
};

window.onload = () => {
  try {
    app.init();
    app.start();
  } catch (err) {
    console.error("Initialization went wrong. Check the console.");
    console.error({ err });
  }
};

window.onunload = () => {
  // save autoconnect preferences
  document.getElementById(app.autoconnectId).checked
    ? localStorage.setItem(app.autoconnectId, true)
    : localStorage.removeItem(app.autoconnectId);
};
