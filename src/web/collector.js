const _collector = {
  networks: [],
  _tick: -1,
  _tickRate: 500, // ms

  chanMsgCounter: chan => {
    const nameElement = chan.querySelector(".name");
    const badgeElement = chan.querySelector(".badge");
    return {
      name: nameElement ? nameElement.innerHTML : null,
      count: badgeElement ? badgeElement.innerHTML : null,
      isHighlighted: !!chan.querySelector(".badge.highlight")
    };
  },

  parseNetwork: networkEl => {
    const lobby = networkEl.querySelector(".chan.lobby");
    const channels = networkEl.querySelectorAll(".channels .chan.channel");
    const queries = networkEl.querySelectorAll(".channels .chan.query");

    const network = {
      nick: networkEl.getAttribute("data-nick") || "",
      name: (lobby && lobby.getAttribute("data-name")) || "",
      channels: [],
      queries: []
    };

    channels &&
      channels.forEach(chan =>
        network.channels.push(_collector.chanMsgCounter(chan))
      );
    queries &&
      queries.forEach(chan =>
        network.queries.push(_collector.chanMsgCounter(chan))
      );

    return network;
  },

  getActiveNetworkElement: activeChanNameElement => {
    const activeChanElement = activeChanNameElement
      ? activeChanNameElement.parentNode
      : null;

    const activeChannelsList = activeChanElement
      ? activeChanElement.parentNode
      : null;

    const activeNetworkElement = activeChannelsList
      ? activeChannelsList.parentNode
      : null;

    const activeNetworkNameElements = activeNetworkElement
      ? activeNetworkElement.getElementsByClassName("lobby")
      : null;

    return activeNetworkNameElements && activeNetworkNameElements.length
      ? activeNetworkNameElements[0]
      : null;
  },

  tick: () => {
    _collector.networks = [];

    const networksNode = document.getElementsByClassName("networks");
    if (networksNode.length > 0) {
      const networks = networksNode[0].getElementsByClassName("network");
      for (let i = 0; i < networks.length; ++i) {
        _collector.networks.push(_collector.parseNetwork(networks[i]));
      }
    }

    const activeChanNameElement = document.querySelector(
      ".channels .active .name"
    );

    const activeNetworkNameElement = _collector.getActiveNetworkElement(
      activeChanNameElement
    );

    require("electron").ipcRenderer.send("collector-tick", {
      // global highlight
      hasHighlight: !!document.querySelector(".channels .badge.highlight"),
      // global highlighted message count
      hMsgCount: document.querySelectorAll(".msg.message.highlight").length,
      // global new messages count
      nMsgCount: document.querySelectorAll(".msg.message").length,
      // global selected channel
      activeChanName: activeChanNameElement
        ? activeChanNameElement.innerHTML
        : null,
      // global selected network
      activeNetworkName: activeNetworkNameElement
        ? activeNetworkNameElement.getAttribute("data-name") || null
        : null,
      networks: _collector.networks
    });
    _collector.start();
  },

  start: () => {
    _collector._tick = setTimeout(_collector.tick, _collector._tickRate);
  },

  stop: () => clearTimeout(_collector._tick)
};
